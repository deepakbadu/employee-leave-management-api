const express = require('express');
var cors = require('cors');

const app = express()

app.use(express.urlencoded({ extended: false }))

app.use(function (req, res, next) {
  res.header("Access-Control-Allow-Origin", " http://localhost:3000");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});
// parse application/json
app.use(express.json())

app.use(cors());
const routers = require('./app/routes/routers')
app.use(routers)

const conn = require('./database');

app.listen(8000, () => {
  console.log('Running on port 8000')
})


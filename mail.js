let nodeMailer = require('nodemailer');

let transporter = nodeMailer.createTransport({
  host: 'smtp.zoho.com',
  port: 465,
  secure: true,  //true for 465 port, false for other ports
  auth: {
    user: 'employeeleavemanagement@zohomail.com',
    pass: 'ab@123cde'
  }
});

module.exports = {
  transporter: transporter
}

const express = require('express')
const router = express()
const admin = require('../controllers/login.controller')
const employee = require('../controllers/employee.contoller')
const department = require('../controllers/department.controller')
const leaveApplication = require('../controllers/leaveApplication.controller')
const leaveType = require('../controllers/leaveType.controller')
const passport = require('../configure/passport')

router.post('/employee', employee.addEmployee)
router.get('/employee/:id', employee.getEmployee)
router.get('/employee', employee.getEmployees)
router.patch('/employee/:id', employee.updateEmployee)
router.delete('/employee/:id', employee.deleteEmployee)
router.get('/employee-count', employee.countEmployee)
router.patch('/change-password/:id', employee.changePassword)


router.post('/department', department.addDepartment)
router.get('/department', department.getDepartment)
router.delete('/department/:id', department.deleteDepartment)
router.get('/department-count', department.countDepartment)


router.post('/leave-application', leaveApplication.addLeaveApplication)
router.get('/leave-application', leaveApplication.getLeaveApplications)
router.patch('/leave-application/:id', leaveApplication.updateLeaveApplication)
router.get('/leave-application/:id', leaveApplication.getLeaveApplication)
router.get('/pending-leaves', leaveApplication.getPendingLeaves)
router.get('/approved-leaves', leaveApplication.getApprovedLeaves)
router.get('/not-approved-leaves', leaveApplication.getNotApprovedLeaves)
router.get('/individual-leave/:id', leaveApplication.getIndividualLeaveApplication)


router.post('/leave-type', leaveType.addLeaveType)
router.get('/leave-type', leaveType.getLeaveType)
router.delete('/leave-type/:id', leaveType.deleteLeaveType)
router.get('/leave-type-count', leaveType.countLeaveType)

router.post('/login', admin.login)
router.get('/user', admin.getDetails)

module.exports = router;

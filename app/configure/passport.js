var passport = require('passport')
var LocalStrategy = require('passport-local').Strategy
var passportJWT = require('passport-jwt')
var JWTStrategy = passportJWT.Strategy;
var ExtractJWT = passportJWT.ExtractJwt;
var JwtConfig = require('./jwtConfig.js');
var conn = require('../../database')

var bcrypt = require('bcrypt');
const saltRounds = 10;

passport.use(new LocalStrategy({
  usernameField: 'email',
  passwordField: 'password'
},
  function (email, password, done) {
    conn.query("SELECT * FROM employee WHERE email=?", [email], (error, results, fields) => {
      if (results.length == 0) {
        return done(null, false, {
          message: "Incorrect email."
        });
      } else {
        if (password === results[0].password)
          return done(null, results, { message: 'Logged in successfully.', role: results[0].role });
        else
          return done(null, false, { message: 'Incorrect password.', });

      }
    })
  }
));

passport.use(new JWTStrategy({
  jwtFromRequest: ExtractJWT.fromAuthHeaderAsBearerToken(),
  secretOrKey: JwtConfig.secret
},
  function (jwtPayload, cb) {
    return cb(null, jwtPayload)
  }
));
passport.serializeUser(function (user, cb) {
  cb(null, user.id);
});

passport.deserializeUser(function (obj, cb) {
  cb(null, obj);
});

module.exports = passport;


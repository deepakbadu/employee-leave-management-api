const conn = require('../../database')

module.exports = {
  addDepartment: (req, res) => {
    let departmentName = req.body.department_name
    conn.query('insert into department (department_name) values (?)',
      [departmentName], (err, data) => {
        if (data) {
          res.json({
            data: req.body
          })
        }
      })
  },
  getDepartment: (req, res) => {
    conn.query('select * from department', (err, data) => {
      if (data) {
        res.status(200).json({ data: data })
      }
    })
  },
  deleteDepartment: (req, res) => {
    let id = req.params.id
    conn.query('delete from department where id=?', [id], (err, data) => {
      if (err) {
        res.json(err)
      } else {
        res.status(204).json({ status: 'Deleted successfully.' });
      }
    })
  },
  countDepartment: (req, res) => {
    conn.query('select count(id) as totalDepartment from department', (err, data) => {
      if (data) {
        res.status(200).json({ data: data })
      }
    })
  },
}
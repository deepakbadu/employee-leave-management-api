const conn = require('../../database')
let mail = require('../../mail')
let randomPassword = require('../middleware/random.password.generator')

module.exports = {
  addEmployee: (req, res) => {
    conn.query("SELECT * from employee where email=?", [req.body.email], function (err, data) {
      if (err) {
        res.json({ status: err })
      } else {
        if (data && data.length > 0) {
          res.json('email already taken')
        } else {
          let name = req.body.name
          let email = req.body.email
          let address = req.body.address
          let department = req.body.department
          let city = req.body.city
          let birthday = req.body.birthday
          let gender = req.body.gender
          let phone = req.body.phone
          let password = randomPassword.generateRandomPassword()

          let mailOptions = {
            from: '"Employee Leave Management" <employeeleavemanagement@zohomail.com>',
            to: email,
            subject: 'Employee Login Information',
            html: `<p>Hello ${name},</p>
                  <div>
                      Welcome to Employee Leave Management System.
                      Your login details are:
                      <p>
                        email: ${email}
                      </p>
                      <p>
                      password: ${password}
                      </p> 
                  </div>
      
                  <div>
                      For better security please change your password
                      as soon as possible.
                  </div>
      
                  <div>
                    <p>
                      Thank you,
                    </p>
                    <p>
                      Employee Leave Management Team.
                    </p>
                  </div>
              `
          };
          conn.query('insert into employee (name,email,address,department,city,birthday,gender,phone,password,role) values (?,?,?,?,?,?,?,?,?,?)',
            [name, email, address, department, city, birthday, gender, phone, password, 'employee'], (err, data) => {
              if (err) {
                res.json({ status: err })
              } else {
                mail.transporter.sendMail(mailOptions, (error, info) => {
                  if (error) {
                    res.status(400).send({ success: false })
                  } else {
                    res.status(201).json({ data: req.body });
                  }
                });
              }
            })
        }
      }
    })
  },

  getEmployee: (req, res) => {
    let id = req.params.id
    conn.query('select * from employee where id = ?', [id], (err, data) => {
      if (data) {
        res.status(200).json({ data: data })
      }
    })
  },

  getEmployees: (req, res) => {
    conn.query('select * from employee where role="employee" ', (err, data) => {
      if (data) {
        res.status(200).json({ data: data })
      }
    })
  },
  updateEmployee: (req, res) => {
    let name = req.body.name
    let email = req.body.email
    let address = req.body.address
    let department = req.body.department
    let city = req.body.city
    let birthday = req.body.birthday
    let gender = req.body.gender
    let phone = req.body.phone
    let id = req.params.id
    conn.query('update employee set name=?,email=?,address=?,department=?,city=?,birthday=?,gender=?,phone=? where id=?',
      [name, email, address, department, city, birthday, gender, phone, id], (err, data) => {
        if (err)
          res.json(err)
        else
          res.status(200).json({ data: req.body })
      })
  },

  deleteEmployee: (req, res) => {
    let id = req.params.id
    conn.query('delete from employee where id=?', [id], (err, data) => {
      if (err) {
        res.json(err)
      } else {
        res.status(204).json({ status: 'Deleted successfully.' });
      }
    })
  },
  changePassword: (req, res) => {
    let password = req.body.password
    let newPassword = req.body.newPassword
    let id = req.params.id
    conn.query('select password from employee where id=?', [id], (err, data) => {
      if (err) {
        res.json(err)
      } else {
        let currentPassword = data[0].password
        if (password === currentPassword) {
          conn.query('update employee set password=? where id=?', [newPassword, id], (err, data) => {
            if (err) {
              res.json(err)
            } else {
              res.status(204).json({ status: 'password updated successfully!!.' });
            }
          })
        }
      }
    })
  },
  countEmployee: (req, res) => {
    conn.query('select count(id)  AS numberOfEmployee from employee', (err, data) => {
      if (data) {
        res.status(200).json({ data: data })
      }
    })
  }
}
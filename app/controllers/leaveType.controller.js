const conn = require('../../database')

module.exports = {
  addLeaveType: (req, res) => {
    const leave_type = req.body.leave_type
    const description = req.body.description

    conn.query('insert into leaveType (leave_type, description) values (?,?)',
      [leave_type, description], (err, data) => {
        if (err) {
          console.log(err)
        } else {
          res.json({
            data: req.body
          })
        }
      })
  },

  getLeaveType: (req, res) => {
    conn.query('select *  from leaveType', (err, data) => {
      if (data) {
        res.status(200).json({ data: data })
      }
    })
  },
  deleteLeaveType: (req, res) => {
    let id = req.params.id
    conn.query('delete from leaveType where id=?', [id], (err, data) => {
      if (err) {
        res.json(err)
      } else {
        res.status(204).json({ status: 'Deleted successfully.' });
      }
    })
  },
  countLeaveType: (req, res) => {
    conn.query('select count(id)  AS numberOfLeaveType from leaveType', (err, data) => {
      if (data) {
        res.status(200).json({ data: data })
      }
    })
  }
}
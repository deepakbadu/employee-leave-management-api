const conn = require('../../database')
const mail = require('../../mail')
module.exports = {
  addLeaveApplication: (req, res) => {
    const leaveType = req.body.leave_type
    const emp_description = req.body.emp_description
    const starting_date = req.body.starting_date
    const ending_date = req.body.ending_date
    const emp_id = req.body.emp_id
    conn.query('insert into leaveApplication (leave_type,emp_description,admin_description,starting_date,ending_date,status,emp_id) values(?,?,?,?,?,?,?)',
      [leaveType, emp_description, 'Waiting for Approval', starting_date, ending_date, 'Pending', emp_id], (err, data) => {
        if (err) {
          console.log(err)
        } else {
          res.json({
            data: req.body
          })
        }
      })
  },
  getLeaveApplications: (req, res) => {
    conn.query("SELECT leaveApplication.*,employee.name,employee.email FROM leaveApplication INNER JOIN employee ON leaveApplication.emp_id = employee.id", (err, data) => {
      if (err) {
        console.log(err)
      } else {
        res.status(200).json({ data: data })
      }
    })
  },
  updateLeaveApplication: (req, res) => {
    let id = req.params.id
    const admin_description = req.body.admin_description
    const status = req.body.status
    conn.query("SELECT employee.name,employee.email FROM leaveApplication INNER JOIN employee ON leaveApplication.emp_id = employee.id where leaveApplication.id=?", [id], (err, data) => {
      if (err) {
        console.log(err)
      } else {
        console.log(data)
        let name = data[0].name
        let email = data[0].email
        console.log(email, name)
        let mailOptions = {
          from: '"Employee Leave Management" <employeeleavemanagement@zohomail.com>',
          to: email,
          subject: 'Leave Application Status',
          html: `<p>Hello, ${name},</p>
              <div>
                  Your Application Modified by admin:
                  <p>
                    status: ${status}
                  </p>
                  <p>
                  admin description: ${admin_description}
                  </p> 
              </div>
    
              <div>
                  For more information you can visit our website.
              </div>
    
              <div>
                <p>
                  Thank you,
                </p>
                <p>
                  Employee Leave Management Team.
                </p>
              </div>
          `
        };
        conn.query('update leaveApplication set admin_description=?,status=? where id=?',
          [admin_description, status, id], (err, data) => {
            if (err) {
              res.json(err)
            }
            else {
              mail.transporter.sendMail(mailOptions, (error, info) => {
                if (error) {
                  res.status(400).send({ success: false })
                } else {
                  res.status(201).json({ data: req.body });
                }
              });
            }
          })
      }
    })
  },
  getLeaveApplication: (req, res) => {
    let id = req.params.id
    conn.query("SELECT leaveApplication.*,employee.name,employee.email FROM leaveApplication INNER JOIN employee ON leaveApplication.emp_id = employee.id where leaveApplication.id=?", [id], (err, data) => {
      if (err) {
        console.log(err)
      } else {
        res.status(200).json({ data: data })
      }
    })
  },
  getIndividualLeaveApplication: (req, res) => {
    let emp_id = req.params.id
    conn.query("SELECT leaveApplication.*,employee.name,employee.email FROM leaveApplication INNER JOIN employee ON leaveApplication.emp_id = employee.id where leaveApplication.emp_id=? order by id desc", [emp_id], (err, data) => {
      if (err) {
        console.log(err)
      } else {
        res.status(200).json({ data: data })
      }
    })
  },
  deleteLeaveApplication: (req, res) => {
    let id = req.params.id
    conn.query("delete from leaveApplication where id=? where id=?", [id], (err, data) => {
      if (err) {
        console.log(err)
      } else {
        res.status(200).json({ data: data })
      }
    })
  },
  getPendingLeaves: (req, res) => {
    conn.query("SELECT leaveApplication.*,employee.name,employee.email FROM leaveApplication INNER JOIN employee ON leaveApplication.emp_id = employee.id where leaveApplication.status = 'Pending' order by id desc ", (err, data) => {
      if (err) {
        console.log(err)
      } else {
        res.status(200).json({ data: data })
      }
    })
  },
  getApprovedLeaves: (req, res) => {
    conn.query("SELECT leaveApplication.*,employee.name,employee.email FROM leaveApplication INNER JOIN employee ON leaveApplication.emp_id = employee.id where leaveApplication.status = 'Approved' order by id desc", (err, data) => {
      if (err) {
        console.log(err)
      } else {
        res.status(200).json({ data: data })
      }
    })
  },
  getNotApprovedLeaves: (req, res) => {
    conn.query("SELECT leaveApplication.*,employee.name,employee.email FROM leaveApplication INNER JOIN employee ON leaveApplication.emp_id = employee.id where leaveApplication.status = 'Not Approved' order by id desc", (err, data) => {
      if (err) {
        console.log(err)
      } else {
        res.status(200).json({ data: data })
      }
    })
  },

}